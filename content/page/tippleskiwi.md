---
title: tipples.kiwi
date: 2018-08-10
comments: false
tags: ["projects", "tipples", "PWA"]
---

# tipples.kiwi

## Summary

[tipples.kiwi](https://tipples.kiwi/) is a directory and blog about tipples attractions in New Zealand.  It's main focus is on Wineries with cellar doors and Craft Breweries with tasting and tours.

This site is created with Hugo and designed to be a [PWA](/post/2018-07-23-hugo-pwa/) for Android and iPhone.  The site is hosted on [Netlify](https://netlify.com/) with [CloudFlare](https://cloudflare.com/) CDN and the code is hosted on [GitLab](https://gitlab.com/yankiwi/).

## Details

**URL:**        [https://tipples.kiwi/](https://tipples.kiwi/)
**Code:**       [GitLab](https://gitlab.com/yankiwi/gl.tipples.kiwi)
**Platform:**   [Hugo](https://gohugo.io/)
**Theme:**     
**Tech:**       [PWA](/post/2018-07-23-hugo-pwa/), [CloudFlare](https://cloudflare.com/)
