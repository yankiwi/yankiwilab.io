---
title: About Me
date: 2018-06-24
comments: false
---

I’m Patrick.  A Yank living and working in Auckland, NZ.  I've done IT work most of my life having worked for a variety of large organziations.  I currently specialize in networking, enterprise and carrier grade Wi-Fi systems.  I enjoy work, FOSS, cycling, running, cars, coffee, wine and craft beer.

I don't know many people in NZ but I'm interested in meeting and taking on projects with people with similar interests.  If that's you, contact me.