---
title: First Post
date: 2018-07-20
tags: ["hugo", "gitlab", "cloudflare"]
---

This is my first post on [yan.kiwi](https://yan.kiwi/).  A site using:
 * [Hugo](https://gohugo.io/) 
 * [GitLab Pages](https://gitlab.com/pages)
 * [CloudFlare](https://www.cloudflare.com/)

I cloned the GitLab Pages Hugo [example site](https://gitlab.com/pages/hugo/) which includes the nessesary files to build static files using pipelines on GitLab's CI/CD.  It's quite a slick build process.  I've found that even commiting via the GitLab Web IDE is reasonable.  Not a replacement for running hugo server locally while editing in your favorite IDE, but good enough that I can leave the personal laptop at home while I travel.

As of the time of this write, I'm using a heavily modifed version of [Calin Tataru](https://calintat.github.io/)'s [Minimal Theme](https://github.com/calintat/minimal).  Some of the changes include upgrading to Bootstrap 4 and various tweaks to improve Lighthouse scores.

Finally, caching and SSL is handled by Cloudflare.  Many thanks to [JR](https://liveaverage.com/) for his [post](https://liveaverage.com/blog/make-development-easy-with-cloudflare-universal-ssl/) on that subject. 